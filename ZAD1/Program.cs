﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {

            Note note1 = new Note("Title1", "Text1");
            Note note2 = new Note("Title2", "Text2");
            Note note3 = new Note("Title3", "Text3");

            Notebook notebook = new Notebook();
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);

            IAbstractIterator n = notebook.GetIterator();
            for (int i = 0; i < notebook.Count; i++)
            {
                n.Current.Show();
                n.Next();
            }        
        }
    }
}
