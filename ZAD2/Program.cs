﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product1 = new Product("Biljeznica a4 - linije", 7.40);
            Product product2 = new Product("CASIO fx-991EX", 250.0);
            Product product3 = new Product("Ruksak", 125.0);

            Box box = new Box();
            box.AddProduct(product1);
            box.AddProduct(product2);
            box.AddProduct(product3);

            IAbstractIterator n = box.GetIterator();
            for(int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(n.Current.ToString());
                n.Next();
            }
        }
    }
}
